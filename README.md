<h1>Installation</h1>
This Webapp can be deployed with docker.<br>

First you have create the .env files in /docker. For this use the templates <code>.env-database.dist</code> and <code>.env.web.dist</code>, remove the <code>.dist</code> suffix and change the passwords in the files.<br><br> 
Now you can build the docker containers. Run:<br>
<code>make docker-build</code>
<br><br>
after building the Containers you can run the command for starting the Server:<br>
<code>make docker-run</codemake>
