<?php

namespace App\Service;

use App\Entity\TimeTableModification;
use DateTime;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class ImportService
{

    public function ImportXml(UploadedFile $File)
    {
        if (file_exists($File)) {
            //begin parsing Data
            $xml = simplexml_load_file($File);

            //Data Parsing
            $dateString = $xml->xpath("/vp/kopf/titel")[0]; //Format: Freitag, 23. August 2019
            //Remove day names
            $dateString = str_replace(["Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag"], "", $dateString);
            $dateString = str_replace([".", ", "], "", $dateString);

            //Convert month name to number
            $dateString = str_replace("Januar", "01", $dateString);
            $dateString = str_replace("Februar", "02", $dateString);
            $dateString = str_replace("März", "03", $dateString);
            $dateString = str_replace("April", "04", $dateString);
            $dateString = str_replace("Mai", "05", $dateString);
            $dateString = str_replace("Juni", "06", $dateString);
            $dateString = str_replace("Juli", "07", $dateString);
            $dateString = str_replace("August", "08", $dateString);
            $dateString = str_replace("September", "09", $dateString);
            $dateString = str_replace("Oktober", "10", $dateString);
            $dateString = str_replace("November", "11", $dateString);
            $dateString = str_replace("Dezember", "12", $dateString);

            $date = DateTime::createFromFormat("d m Y", $dateString)->setTime(0, 0, 0, 0);

            //parsing actual content
            $ttmod = array();
            foreach ($xml->xpath("/vp/haupt/aktion") as $a) {
                $t = new TimeTableModification();

                $t->setDate($date);
                $t->setTime($a->stunde);
                $t->setSubject($a->fach);
                $t->setTeacher($a->lehrer);
                $t->setClass($a->klasse);
                $t->setChangedSubject($a->vfach);
                $t->setChangedTeacher($a->vlehrer);
                $t->setChangedRoom($a->vraum);
                $t->setInfo($a->info);

                $ttmod[] = $t;
            }
            return $ttmod;
        }
    }
}
