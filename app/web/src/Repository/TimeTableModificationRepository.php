<?php

namespace App\Repository;

use App\Entity\TimeTableModification;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TimeTableModification|null find($id, $lockMode = null, $lockVersion = null)
 * @method TimeTableModification|null findOneBy(array $criteria, array $orderBy = null)
 * @method TimeTableModification[]    findAll()
 * @method TimeTableModification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TimeTableModificationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TimeTableModification::class);
    }


    /**
     * @param $dateTime
     * @return TimeTableModification[]
     */
    public function findByDate(DateTime $dateTime)
    {
        $date = $dateTime->format("Y-m-d"); //probably not really a great solution, but this is needed, otherwise I cant get the Objects
        return $this->createQueryBuilder('a')
            ->andWhere('a.date = :date')
            ->setParameter('date', $date)
            ->orderBy('a.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return DateTime[]
     */
    public function getDates() {
        return $this->createQueryBuilder('ttm')
            ->select('ttm.date')
            ->orderBy('ttm.date', 'ASC')
            ->distinct()
            ->getQuery()
            ->getResult();
    }



public function countByDate(DateTime $dateTime) //TODO: Move to Utility
    {
        $date = $dateTime->format("Y-m-d"); //probably not really a great solution, but this is needed, otherwise I cant get the Objects
        $em = $this->getEntityManager();
        return $em
            ->getRepository(TimeTableModification::class)
            ->count(['date' => $date]);
    }


    public function removeByDate(DateTime $dateTime) //TODO: Move to Utility
    {
        $em = $this->getEntityManager();
        $repo = $em->getRepository(TimeTableModification::class)
            ->findByDate($dateTime);
        foreach ($repo as $a) {
                //tryCatch was proposed by PHPStorm
                try {
                    $em->remove($a);
                } catch (ORMException $e) {
            }
        }
        //tryCatch was proposed by PHPStorm
        try {
            $em->flush();
        } catch (OptimisticLockException $e) {
        } catch (ORMException $e) {
        }
    }

    public function existsByDate(DateTime $dateTime)
    {
        return $this->countByDate($dateTime) > 0;
    }
}
