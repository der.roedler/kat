<?php

namespace App\Controller;

use App\Entity\Notification;
use App\Form\AddNotificationsType;
use App\Form\EditNotificationsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Exception\LogicException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


class NotificationAdminController extends AbstractController
{
    /**
     * @Route("/admin/notifications/index")
     * @return Response
     */
    public function Index()
    {
        $repository = $this->getDoctrine()->getRepository(Notification::class)->findAll();
        $notifications = array();

        foreach ($repository as $r) {
            $notification = [
                "date" => $r->getDate()->format("Y-m-d H:i:s"),
                "header" => $r->getHeader(),
                "content" => $r->getContent(),
                "showTeacherArea" => $r->getShowTeacherArea(),
                "showStudentArea" => $r->getshowStudentArea(),
                "id" => $r->getId()
            ];
            $notifications[] = $notification;
        }

//        return new Response(var_dump($notifications));
        return $this->render("notification/index.html.twig", [
            'notifications' => $notifications
        ]);
    }


    /**
     * @Route("/admin/notifications/add")
     * @param Request $request
     * @return Response
     * @throws LogicException
     */
    public function notificationsAdd(Request $request)
    {
        // creates a task and gives it some dummy data for this example
        $notification = new Notification();
        $notification->setDate(date_create());
        $form = $this->createForm(AddNotificationsType::class, $notification);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $notification = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($notification);
            $entityManager->flush();

            return new Response("Success");
        }

        return $this->render('timeTableModification/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param $id
     * @Route("/admin/notifications/delete/{id}")
     * @return Response
     */
    public function notificationsDelete($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $notification = $this->getDoctrine()->getRepository(Notification::class)->find($id);
        $entityManager->remove($notification);
        $entityManager->flush();
        return $this->redirectToRoute('app_notificationadmin_index');

    }

    /**
     * @param Request $request
     * @param $id
     * @return Response
     * @Route("/admin/notifications/edit/{id}")
     */
    public function notificationsEdit(Request $request, $id)
    {
        // creates a task and gives it some dummy data for this example
        $entityManager = $this->getDoctrine()->getManager();
        $notification = $this->getDoctrine()->getRepository(Notification::class)->find($id);

        $form = $this->createForm(EditNotificationsType::class, $notification);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $updatedNotification = $form->getData();

            $notification = $updatedNotification;
            $entityManager->flush();

            return new Response("Success");
        }

        return $this->render('timeTableModification/new.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/admin/notifications/dump")
     */
    public function notificationsDump()
    {
        $repository = $this->getDoctrine()->getRepository(Notification::class);
        $ttmod = $repository->findAll();

        return new Response(var_dump($ttmod));
    }

}
