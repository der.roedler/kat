<?php

namespace App\Controller;

use App\Entity\Notification;
use DateTime;
use DateInterval;
use Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\TimeTableModification;


class DisplayController extends AbstractController
{
    /**
     * @Route("/show")
     * @Route("")
     */
    public function index()
    {
        return $this->redirectToRoute('app_display_showstudent');
    }

    /**
     * @Route("/show/student")
     */
    public function showStudent()
    {
        // Get TimeTableModification and convert to usable Datastructure
        $timeTableModificationRepository = $this->getDoctrine()->getRepository(TimeTableModification::class);
        $timeTableModificationDates = $timeTableModificationRepository->getDates();
        $timeTableModification = array();
        foreach ($timeTableModificationDates as $d) {
            $date = $d["date"];
            $timeTableModification[] = array(
                "date" => $date->format("Y-m-d"),
                "data" => $timeTableModificationRepository->findBydate($date)
            );
        }

        // get Notification and convert to usable Data
        $notificationRepository = $this->getDoctrine()->getRepository(Notification::class);
        $notification = $notificationRepository->findStudentNotifications();
        $notifications = array();
        foreach ($notification as $n) {
            $notifications[] = array(
                "header" => $n->getHeader(),
                "content" => $n->getContent()
            );
        }

//        return new Response(var_dump($notifications));
        return $this->render('display/display.html.twig', [
            "timetablemodifications" => $timeTableModification,
            "notifications" => $notifications
        ]);
    }

    /**
     * @Route("/show/teacher")
     */
    public function showTeacher()
    {
        // Get TimeTableModification and convert to usable Datastructure
        $timeTableModificationRepository = $this->getDoctrine()->getRepository(TimeTableModification::class);
        $timeTableModificationDates = $timeTableModificationRepository->getDates();
        $timeTableModification = array();
        foreach ($timeTableModificationDates as $d) {
            $date = $d["date"];
            $timeTableModification[] = array(
                "date" => $date->format("Y-m-d"),
                "data" => $timeTableModificationRepository->findBydate($date)
            );
        }

        // get Notification and convert to usable Data
        $notificationRepository = $this->getDoctrine()->getRepository(Notification::class);
        $notification = $notificationRepository->findTeacherNotifications();
        $notifications = array();
        foreach ($notification as $n) {
            $notifications[] = array(
                "header" => $n->getHeader(),
                "content" => $n->getContent()
            );
        }

//        return new Response(var_dump($notifications));
        return $this->render('display/display.html.twig', [
            "timetablemodifications" => $timeTableModification,
            "notifications" => $notifications
        ]);
    }





}
