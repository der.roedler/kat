<?php

namespace App\Controller;

use App\Form\ImportXmlType;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\TimeTableModification;
use App\Form\AddTimeTableModificationType;
use App\Form\EditTimeTableModificationType;
use App\Service\ImportService;


class TimeTableModificationAdminController extends AbstractController
{
    /**
     * @return Response
     * @Route("/admin/timeTableModification/index")
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(TimeTableModification::class)->getDates();

        $dates = array();
        foreach ($repository as $r) {
            $dates[] = $r["date"]->format("Y-m-d");
        }
        return $this->render("timeTableModification/index.html.twig", [
            'dates' => $dates,
        ]);
    }

    /**
     * @Route("/admin/timeTableModification/date/{date}")
     */
    public function timeTableModificationListDate($date)
    {
        $dateTime = DateTime::createFromFormat('Y-m-d', $date);
        $dateTime->setTime(0, 0, 0, 0);
        $repository = $this->getDoctrine()->getRepository(TimeTableModification::class)->findByDate($dateTime);
//        return new Response(var_dump($repository));
        return $this->render('timeTableModification/listDate.html.twig', [
            "date" => $date,
            "repository" => $repository
        ]);
    }


    /**
     * @Route("/admin/timeTableModification/add")
     * @param Request $request
     * @return Response
     */
    public function timeTableModificationAdd(Request $request)
    {
        // creates a task and gives it some dummy data for this example
        $timeTableModification = new TimeTableModification();
        $timeTableModification->setDate(date_create()->setTime(0, 0, 0, 0));
        $timeTableModification->setSubject("MA");
        $timeTableModification->setClass("11");
        $timeTableModification->setTeacher("Foo");
        $timeTableModification->setTime("1-2");
        $timeTableModification->setChangedSubject("Deu");
        $timeTableModification->setChangedRoom("113");
        $timeTableModification->setChangedTeacher("Bar");
        $timeTableModification->setInfo("Das ist ein Test");


        $form = $this->createForm(AddTimeTableModificationType::class, $timeTableModification);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $timeTableModification = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($timeTableModification);
            $entityManager->flush();

            return $this->redirectToRoute('app_timetablemodificationadmin_index');
        }

        return $this->render('timeTableModification/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/timeTableModification/deleteByDate/{date}")
     * @param $date
     * @return Response
     */
    public function timeTableModificationDeleteByDate($date)
    {
        $dateTime = DateTime::createFromFormat('Y-m-d', $date);
        $dateTime->setTime(0, 0, 0, 0);
        $this->getDoctrine()
            ->getRepository(TimeTableModification::class)
            ->removeByDate($dateTime);

        return $this->redirectToRoute('app_timetablemodificationadmin_index');
    }

    /**
     * @param $id
     * @Route("/admin/timeTableModification/deleteById/{id}")
     * @return Response
     */
    public function timeTableModificationDeleteById($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $timeTableModification = $this->getDoctrine()->getRepository(TimeTableModification::class)->find($id);
        $entityManager->remove($timeTableModification);
        $entityManager->flush();
        return $this->redirectToRoute('app_timetablemodificationadmin_index');

    }

    /**
     * @param Request $request
     * @param $id
     * @return Response
     * @Route("/admin/timeTableModification/edit/{id}")
     */
    public function timeTableModificationEdit(Request $request, $id)
    {
        // creates a task and gives it some dummy data for this example
        $entityManager = $this->getDoctrine()->getManager();
        $timeTableModification = $this->getDoctrine()->getRepository(TimeTableModification::class)->find($id);

        $form = $this->createForm(EditTimeTableModificationType::class, $timeTableModification);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $updatedTimeTableModification = $form->getData();

            $entityManager->flush();

            return new Response("Success");
        }

        return $this->render('timeTableModification/edit.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/admin/timeTableModification/importXml")
     * @param Request $request
     * @return Response
     */
    public function timeTableModificationImportXml(Request $request)
    {
        //Do stuff for Form
        $form = $this->createForm(ImportXmlType::class);

        //this is for the Respose
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $data = $form->getData();
            $File = $data['File'];

            $entityManager = $this->getDoctrine()->getManager();

            foreach (importService::ImportXml($File) as $timeTableModification) $entityManager->persist($timeTableModification);
            $entityManager->flush();
            return $this->redirectToRoute('app_timetablemodificationadmin_index');
        }
        return $this->render('timeTableModification/importXml.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/timeTableModification/dump")
     */
    public function timeTableModificationDump()
    {
        $repository = $this->getDoctrine()->getRepository(TimeTableModification::class);
        $timeTableModification = $repository->findAll();

        return new Response(var_dump($timeTableModification));
    }

    /**
     * @Route("/admin/timeTableModification/getdate")
     */
    public function timeTableModificationFindByDate()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $entityManager->getRepository(TimeTableModification::class)->findAll();
        $dates = array();
        foreach ($repository as $a) {
            $date = $a->getDate();
            if (array_search($date, $dates) === false) {
                $dates[] = $date;
            }
        }

        return new Response(var_dump($dates));

    }

}
