<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NotificationsRepository")
 */
class Notification
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $header;

    /**
     * @ORM\Column(type="string", length=280, nullable=true)
     */
    private $content;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $showTeacherArea;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $showStudentArea;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getShowTeacherArea(): ?bool
    {
        return $this->showTeacherArea;
    }

    public function setShowTeacherArea(?bool $showTeacherArea): self
    {
        $this->showTeacherArea = $showTeacherArea;

        return $this;
    }

    public function getShowStudentArea(): ?bool
    {
        return $this->showStudentArea;
    }

    public function setShowStudentArea(?bool $showStudentArea): self
    {
        $this->showStudentArea = $showStudentArea;

        return $this;
    }

    public function getHeader(): ?string
    {
        return $this->header;
    }

    public function setHeader(?string $header): self
    {
        $this->header = $header;

        return $this;
    }
}
