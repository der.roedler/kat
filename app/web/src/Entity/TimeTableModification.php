<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TimeTableModificationRepository")
 */
class TimeTableModification
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date", length=255)
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $time;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $subject;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $teacher;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $class;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $changedSubject;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $changedTeacher;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $changedRoom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $info;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate( $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTime(): ?string
    {
        return $this->time;
    }

    public function setTime(?string $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(?string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getTeacher(): ?string
    {
        return $this->teacher;
    }

    public function setTeacher(?string $teacher): self
    {
        $this->teacher = $teacher;

        return $this;
    }

    public function getClass(): ?string
    {
        return $this->class;
    }

    public function setClass(?string $class): self
    {
        $this->class = $class;

        return $this;
    }

    public function getChangedSubject(): ?string
    {
        return $this->changedSubject;
    }

    public function setChangedSubject(?string $changedSubject): self
    {
        $this->changedSubject = $changedSubject;

        return $this;
    }

    public function getChangedTeacher(): ?string
    {
        return $this->changedTeacher;
    }

    public function setChangedTeacher(?string $changedTeacher): self
    {
        $this->changedTeacher = $changedTeacher;

        return $this;
    }

    public function getChangedRoom(): ?string
    {
        return $this->changedRoom;
    }

    public function setChangedRoom(?string $changedRoom): self
    {
        $this->changedRoom = $changedRoom;

        return $this;
    }

    public function getInfo(): ?string
    {
        return $this->info;
    }

    public function setInfo(?string $info): self
    {
        $this->info = $info;

        return $this;
    }
}
