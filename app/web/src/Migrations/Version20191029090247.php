<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191029090247 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE time_table_modification (id INT AUTO_INCREMENT NOT NULL, date DATE NOT NULL, time VARCHAR(255) DEFAULT NULL, subject VARCHAR(255) DEFAULT NULL, teacher VARCHAR(255) DEFAULT NULL, class VARCHAR(255) DEFAULT NULL, changed_subject VARCHAR(255) DEFAULT NULL, changed_teacher VARCHAR(255) DEFAULT NULL, changed_room VARCHAR(255) DEFAULT NULL, info VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notification (id INT AUTO_INCREMENT NOT NULL, date DATETIME NOT NULL, header VARCHAR(50) DEFAULT NULL, content VARCHAR(280) DEFAULT NULL, show_teacher_area TINYINT(1) DEFAULT NULL, show_student_area TINYINT(1) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE time_table_modification');
        $this->addSql('DROP TABLE notification');
    }
}
