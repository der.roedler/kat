<?php
/**
 * Created by PhpStorm.
 * User: heinrich
 * Date: 10.03.19
 * Time: 12:06
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class EditTimeTableModificationType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', DateType::class)
            ->add('subject', TextType::class)
            ->add('class', TextType::class)
            ->add('teacher', TextType::class)
            ->add('time', TextType::class)
            ->add('changedSubject', TextType::class)
            ->add('changedRoom', TextType::class)
            ->add('changedTeacher', TextType::class)
            ->add('info', TextType::class)
            ->add('save', SubmitType::class, ['label' => 'Eintrag aktualisieren']);
    }
}
