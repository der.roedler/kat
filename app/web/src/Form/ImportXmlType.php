<?php
/**
 * Created by PhpStorm.
 * User: heinrich
 * Date: 10.03.19
 * Time: 12:02
 */
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ImportXmlType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('File', FileType::class)
            ->add('save', SubmitType::class);
    }
}