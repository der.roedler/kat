<?php
/**
 * Created by PhpStorm.
 * User: heinrich
 * Date: 10.03.19
 * Time: 12:06
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class AddNotificationsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', DateTimeType::class)
            ->add('header', TextType::class)
            ->add('content', TextType::class)
            ->add('showTeacherArea', CheckboxType::class, [
                'label'     => "Im Lehrerbereich anzeigen",
                'required'  => false,
            ])
            ->add('showStudentArea', CheckboxType::class, [
                'label'     => "Im Schülerbereich anzeigen",
                'required'  => false,
            ])
            ->add('save', SubmitType::class, ['label' => 'Eintrag erstellen']);
    }
}
